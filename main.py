from scripts.core.convert import Convert

cnv = Convert('E:/unique/scripts/core/data.json')
input_dict = cnv.to_python('E:/unique/scripts/core/data.json')
colors = []


def unique_colors(data):
    if not isinstance(data, str):
        for k, val in data.items():
            if isinstance(val, dict):
                unique_colors(val)
            elif hasattr(val, '__iter__') and not isinstance(val, str):
                for dictitem in val:
                    unique_colors(dictitem)
            elif isinstance(val, str):
                if k == "color":
                    colors.append(val)
                else:
                    if k == "color":
                        colors.append(val)
    return list(set(colors))


print(unique_colors(input_dict))
