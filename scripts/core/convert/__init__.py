import json


class Convert:
    def __init__(self, file):
        self.file = file

    def to_python(self, file):
        with open(file) as json_file:
            data = json.load(json_file)
            return data


